// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>

#define	DIR				-1
#define OUT_WIDTH		1920
#define OUT_HEIGHT		1080
#define OUT_WIDTH_HALF	960
#define OUT_HEIGHT_HALF	540

#define DONUT_WIDTH		2048
#define DONUT_HEIGHT	2048

unsigned int g_image_size_in;
unsigned int g_image_size_out;

using namespace std;

unsigned int * g_image_in;	// use 32 bit integer
unsigned int * g_image_out;	// use 32 bit integer
unsigned int * g_mapping;	// use 32 bit integer

// global variables
//int g_dir    = -1;
int g_r      = 600;

float g_theta  = 60 * (3.14159265/180);
float g_phi    = 135 * (3.14159265/180);
float g_gamma  = 0 * (3.14159265/180);

//int OUT_WIDTH      = 960;
//int OUT_HEIGHT      = 540;

int g_prsrad   = 600;   // Radius for Perspective, Set to ~60% of int g_donrad
int g_donrad   = 1024;  // Radius for full donut
int g_center_x = 1024;  // center of donut
int g_center_y = 1024;  // center of donut

unsigned int g_image_in_x_min = 2048;
unsigned int g_image_in_x_max = 0;
unsigned int g_image_in_y_min = 2048;
unsigned int g_image_in_y_max = 0;

void readSrcDonutFile()
{
	int pixelcount = 0 ;
	std::string line ;
	std::string token;
	std::ifstream infile( "C:\\0\\project\\hummingbird\\test\\donut_bw.txt" ) ;
	unsigned int * image_in = g_image_in;

	if ( infile ) 
	{
		while ( getline( infile , line ) ) 
		{
			if(pixelcount++ >= g_image_size_in)
				break;

			stringstream ss(line);
			while (ss >> token)
			{
				if(pixelcount++ >= g_image_size_in)
					break;

				unsigned char pixel = (unsigned char)atoi(token.c_str());
				*image_in = pixel;
				image_in++;
			}
		}
	}
	infile.close( ) ;
}

void createMapping(int step)
{
	printf ("--------create mapping------------\n"); 

	//unsigned int * image_out = g_image_out;
	unsigned int * mapping = g_mapping;

	float phi = step * (3.14159265/180); 

	float pz2 = sin(g_theta);
	float pz1 = cos(g_theta);
	float px2 = sin(phi);
	float py2 = cos(phi);
	float rts =  g_r * pz2;

	float px1 = rts * py2;
	float px3 = pz1 * py2;

	float py1 = rts * px2;
	float py3 = pz1 * px2;
	
	pz1 = g_r * pz1;

	int r2 = g_r * g_r;
	int dr2 = g_donrad * g_donrad;
	float ux, uy, uz, vx, vy, vz;

	int vdel = OUT_HEIGHT_HALF;

	for( int h = 0; h < OUT_HEIGHT; h++) 
	{
		int udel = - OUT_WIDTH_HALF;

		for (int w = 0; w < OUT_WIDTH; w++) 
		{
			if((h == 0 ) && ( w == 0))
			{
	#ifdef DIR
				ux = px1 + udel * px2 + vdel * px3;
	#else
				ux = px1 + udel * px2 - vdel * px3;
	#endif
 			
	#ifdef DIR
				uy = py1 -udel * py2 + vdel * py3;
	#else
				uy = py1 -udel * py2 - vdel * py3;
	#endif
       
	#ifdef DIR
				uz = -pz1 + vdel * pz2;
	#else
				float z = pz1 + vdel * pz2;
	#endif
				vx = ux;
				vy = uy;
				vz = uz;
			}
			else if(w == 0) // v decrease
			{
				vx -= px3;
				vy -= py3;
				vz -= pz2;

				ux = vx;
				uy = vy;
				uz = vz;
			}
			else	// w increase
			{
				ux += px2;
				uy -= py2;
			}

//			printf("*(%d, %d) x=%d, y=%d, z=%d\n", udel, vdel, x, y, z);
            
#ifdef DIR
			float zclamp = -uz;
#else
			float zclamp = uz;
#endif

			// Debug precomputes
			//float xp1 = (g_r * sin(g_theta) * cos(phi));
			//float xp2 = (1 * cos(g_gamma) * sin(phi)) + 
			//	   (-1 * sin(g_gamma) * g_dir * cos(g_theta) * cos(phi));
			//float xp3 = (-1 * sin(g_gamma) * sin(phi)) +  
			//	   (-1 * cos(g_gamma) * g_dir * cos(g_theta) * cos(phi));

			//float yp1 = (g_r * sin(g_theta) * sin(phi));
			//float yp2 = (-1 * cos(g_gamma) * cos(phi)) +
			//	   (-1 * sin(g_gamma) * g_dir * cos(g_theta) * sin(phi));
			//float yp3 = (1 * sin(g_gamma) * cos(phi)) +
			//	   (-1 * cos(g_gamma) * g_dir * cos(g_theta) * sin(phi));

			//float zp1 = (g_r * g_dir * cos(g_theta));
			//float zp2 = (1 * sin(g_gamma) * sin(g_theta));
			//float zp3 = (1 * cos(g_gamma) * sin(g_theta));

			int dont = udel * udel + vdel * vdel + r2;
			float dont1 = sqrt((float)dont);

#ifdef DIR
			dont1 -= uz;
#else
			dont1 += uz;
#endif 
			dont1 = 1/dont1;
			dont1 = g_prsrad * dont1;

			float xdist = (int)(ux) * dont1;
			float ydist = (int)(uy) * dont1;

			float xdon = g_center_x + xdist;
			float ydon = g_center_y - ydist;
       
			// printf "xp1 = $xp1, xp2 = $xp2, xp3 = $xp3\n";
			// printf "yp1 = float yp1, yp2 = float yp2, yp3 = float yp3\n";
			// printf "zp1 = $zp1, zp2 = $zp2, zp3 = $zp3\n";
			// printf "XDON = $xdon, YDON = float ydon\n";
			// <STDIN>;

			//float xu, xv, yu, yv;
	  //      if (h == 0 && w == 0) 
			//{
	  //         xu = x2 + x4;
	  //         xv = x3 - x5;
	  //         yu =  y4 - y2;
	  //         yv = -y3 - y5;
	  //        // printf STAT "$step $xu $xv float yu float yv\n";
	  //       };
 
			// Check for out of bounds.

			int ixdon = (int)(xdon+0.5);
			int iydon = (int)(ydon+0.5);

			// todo: try remove this condition
			if (((xdist*xdist) + (ydist* ydist)) > dr2) 
			{
			   *mapping = 0;
			} 
			else
			{
				*mapping = ixdon + (iydon << 16);
			}

			mapping++;

			// Print co-ordinates of four corner pixels
			if (h == 0    && w == 0   ) 
			{ 
				if(ixdon < g_image_in_x_min)
					g_image_in_x_min = ixdon;
				if(ixdon > g_image_in_x_max)
					g_image_in_x_max = ixdon;
				if(iydon < g_image_in_y_min)
					g_image_in_y_min = iydon;
				if(iydon > g_image_in_y_max)
					g_image_in_y_max = iydon;

				printf ("XA = %6.2f YA = %6.2f\n", xdon, ydon); 
			};
			if (h == 0    && w == OUT_WIDTH-1) 
			{ 
				if(ixdon < g_image_in_x_min)
					g_image_in_x_min = ixdon;
				if(ixdon > g_image_in_x_max)
					g_image_in_x_max = ixdon;
				if(iydon < g_image_in_y_min)
					g_image_in_y_min = iydon;
				if(iydon > g_image_in_y_max)
					g_image_in_y_max = iydon;

				printf ("XB = %6.2f YB = %6.2f\n", xdon, ydon); 
			};
			if (h == OUT_HEIGHT-1 && w == OUT_WIDTH-1) 
			{ 
				if(ixdon < g_image_in_x_min)
					g_image_in_x_min = ixdon;
				if(ixdon > g_image_in_x_max)
					g_image_in_x_max = ixdon;
				if(iydon < g_image_in_y_min)
					g_image_in_y_min = iydon;
				if(iydon > g_image_in_y_max)
					g_image_in_y_max = iydon;

				printf ("XC = %6.2f YC = %6.2f\n", xdon, ydon); 
				printf ("-------------------------------\n"); 
			};
			if (h == OUT_HEIGHT-1 && w == 0   ) 
			{ 
				if(ixdon < g_image_in_x_min)
					g_image_in_x_min = ixdon;
				if(ixdon > g_image_in_x_max)
					g_image_in_x_max = ixdon;
				if(iydon < g_image_in_y_min)
					g_image_in_y_min = iydon;
				if(iydon > g_image_in_y_max)
					g_image_in_y_max = iydon;

				printf ("XD = %6.2f YD = %6.2f\n", xdon, ydon); 
			};

			udel++;

			// Insert border colors for debug image
			//if (h == 0)                 { $border{int(float ydon+0.5)}{int($xdon+0.5)} = 1; };
			//if (h != 0 && w == OUT_WIDTH-1) { $border{int(float ydon+0.5)}{int($xdon+0.5)} = 2; };
			//if (h == OUT_HEIGHT-1)              { $border{int(float ydon+0.5)}{int($xdon+0.5)} = 3; };
			//if (h != 0 && line != OUT_HEIGHT-1 && w == 0) { $border{int(float ydon+0.5)}{int($xdon+0.5)} = 4; };
		}
		vdel--;
	}

	printf ("(%d, %d), (%d, %d), (%d, %d), (%d, %d)\n", g_image_in_x_min, g_image_in_y_min,
														g_image_in_x_max, g_image_in_y_min,
														g_image_in_x_min, g_image_in_y_max,
														g_image_in_x_max, g_image_in_y_max); 
}

#define SINE		193
#define COSINE		203
#define MUL_FLOAT	2
#define ASSIGN_V	1
#define PLUS		0.25
#define CWRITE		1
#define SQRT		1
#define DIVIDBY1	1
#define IFCON		1
#define FORCON		1
#define COMPARE		1

void createMapping_profile()
{
	float pz2 = SINE;
	float pz1 = COSINE;
	float px2 = SINE;
	float py2 = COSINE;
	float rts =  MUL_FLOAT;

	float px1 = MUL_FLOAT;
	float px3 = MUL_FLOAT;

	float py1 = MUL_FLOAT;
	float py3 = MUL_FLOAT;

	float cycles = pz2 + pz1 + px2 + py2 + rts + px1 + px3 + py1 + py3;

	pz1 = MUL_FLOAT;

	int r2 = MUL_FLOAT;
	int dr2 = MUL_FLOAT;

	cycles += pz1 + r2 + dr2;

	for( int h = OUT_HEIGHT; h > 0; h--) 
	{
		cycles += PLUS;
		cycles += FORCON;

		for (int w = - OUT_WIDTH_HALF; w < OUT_WIDTH - OUT_WIDTH_HALF; w++) 
		{
			cycles += PLUS;
			cycles += FORCON;
			cycles += IFCON;

			if((h == 0 ) && ( w == 0))
			{
	#ifdef DIR
				cycles += PLUS + MUL_FLOAT + PLUS + MUL_FLOAT;
	#else
				ux = px1 + udel * px2 - vdel * px3;
	#endif
 			
	#ifdef DIR
				cycles += PLUS + MUL_FLOAT + PLUS + MUL_FLOAT;
	#else
				uy = py1 -udel * py2 - vdel * py3;
	#endif
       
	#ifdef DIR
				cycles += PLUS + MUL_FLOAT;
	#else
				float z = pz1 + vdel * pz2;
	#endif
				//vx = ux;
				//vy = uy;
				//vz = uz;
			}
			else if(w == 0) // v decrease
			{
				cycles += PLUS;
				cycles += PLUS;
				cycles += PLUS;

				//ux = vx;
				//uy = vy;
				//uz = vz;
			}
			else	// w increase
			{
				cycles += PLUS;
				cycles += PLUS;
			}
            
#ifdef DIR
			//float zclamp = -uz;
#else
			float zclamp = uz;
#endif
			cycles += MUL_FLOAT + PLUS + MUL_FLOAT + PLUS;
			cycles += SQRT;

#ifdef DIR
			//dont1 -= uz;
#else
			dont1 += uz;
#endif 
			cycles += DIVIDBY1;
			cycles += MUL_FLOAT;

			cycles += MUL_FLOAT;
			cycles += MUL_FLOAT;

			cycles += PLUS;
			cycles += PLUS;

			cycles += PLUS;
			cycles += PLUS;

			//// todo: try remove this condition
			//if (((xdist*xdist) + (ydist* ydist)) > dr2) 
			//{
			//   *mapping = 0;
			//} 
			//else
			{
				cycles += PLUS;
				cycles += CWRITE;
			}

			//mapping++;
		}
	}

	printf ("cycles = %f\n", cycles); 
}

void createPersp()
{
	unsigned int * image_out = g_image_out;
	unsigned int * mapping = g_mapping;

	for( int h = 0; h < OUT_HEIGHT; h++) 
	{
		for (int w = 0; w < OUT_WIDTH; w++) 
		{
			int index = *mapping;

			// todo: try remove this condition
			if (index == 0) 
			{

			   *image_out = 0; 
			} 
			else 
			{        
				index = (index & 0xffff) + ((index >> 16) & 0xffff) * DONUT_WIDTH;
			   *image_out = *(g_image_in + index);
			};

			image_out++;
			mapping++;
		}
	}
}

void writeDonut()
{
	unsigned int * image_in = g_image_in;
	FILE *f;
	int filesize = 54 + 3* DONUT_WIDTH* DONUT_HEIGHT;

	unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
	unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
	unsigned char bmppad[3] = {0,0,0};

	bmpfileheader[ 2] = (unsigned char)(filesize    );
	bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
	bmpfileheader[ 4] = (unsigned char)(filesize>>16);
	bmpfileheader[ 5] = (unsigned char)(filesize>>24);

	bmpinfoheader[ 4] = (unsigned char)(       DONUT_WIDTH    );
	bmpinfoheader[ 5] = (unsigned char)(       DONUT_WIDTH>> 8);
	bmpinfoheader[ 6] = (unsigned char)(       DONUT_WIDTH>>16);
	bmpinfoheader[ 7] = (unsigned char)(       DONUT_WIDTH>>24);

	int rh = -DONUT_HEIGHT;
	bmpinfoheader[ 8] = (unsigned char)(       rh    );
	bmpinfoheader[ 9] = (unsigned char)(       rh>> 8);
	bmpinfoheader[10] = (unsigned char)(       rh>>16);
	bmpinfoheader[11] = (unsigned char)(       rh>>24);
	
	f = fopen("C:\\0\\project\\hummingbird\\test\\donut.bmp","wb");

	fwrite(bmpfileheader,1,14,f);
	fwrite(bmpinfoheader,1,40,f);

	for( int h = 0; h < DONUT_HEIGHT; h++) 
	{
		for(int w = 0; w < DONUT_WIDTH; w++)
		{
			fwrite((void*)image_in, 1, 1, f);
			fwrite((void*)image_in, 1, 1, f);
			fwrite((void*)image_in, 1, 1, f);
			image_in++;
		}
		fwrite(bmppad,1,(4-(DONUT_WIDTH*3)%4)%4,f);
	}

	fclose(f);
}

void writePersp(int step)
{
	unsigned int * image_out = g_image_out;
	FILE *f;
	int filesize = 54 + 3* OUT_WIDTH* OUT_HEIGHT;

	unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
	unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
	unsigned char bmppad[3] = {0,0,0};

	bmpfileheader[ 2] = (unsigned char)(filesize    );
	bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
	bmpfileheader[ 4] = (unsigned char)(filesize>>16);
	bmpfileheader[ 5] = (unsigned char)(filesize>>24);

	bmpinfoheader[ 4] = (unsigned char)(       OUT_WIDTH    );
	bmpinfoheader[ 5] = (unsigned char)(       OUT_WIDTH>> 8);
	bmpinfoheader[ 6] = (unsigned char)(       OUT_WIDTH>>16);
	bmpinfoheader[ 7] = (unsigned char)(       OUT_WIDTH>>24);

	int rh = -OUT_HEIGHT;
	bmpinfoheader[ 8] = (unsigned char)(       rh    );
	bmpinfoheader[ 9] = (unsigned char)(       rh>> 8);
	bmpinfoheader[10] = (unsigned char)(       rh>>16);
	bmpinfoheader[11] = (unsigned char)(       rh>>24);
	
	char ofile[256];
	sprintf (ofile, "C:\\0\\project\\hummingbird\\test\\persp_%d.bmp", step);
	f = fopen(ofile,"wb");

	fwrite(bmpfileheader,1,14,f);
	fwrite(bmpinfoheader,1,40,f);

	for( int h = 0; h < OUT_HEIGHT; h++) 
	{
		for(int w = 0; w < OUT_WIDTH; w++)
		{
			fwrite((void*)image_out, 1, 1, f);
			fwrite((void*)image_out, 1, 1, f);
			fwrite((void*)image_out, 1, 1, f);
			image_out++;
		}
		fwrite(bmppad,1,(4-(OUT_WIDTH*3)%4)%4,f);
	}

	fclose(f);
}
int _tmain(int argc, _TCHAR* argv[])
{
	g_image_size_in = DONUT_WIDTH * DONUT_HEIGHT;
	g_image_size_out = OUT_WIDTH * OUT_HEIGHT;

	g_image_in = new unsigned int[g_image_size_in];
	g_image_out = new unsigned int[g_image_size_out];
	g_mapping = new unsigned int[g_image_size_out]; 

	//writeDonut();

	int step = 135;
	//for (int step = 0; step <= 360; step += 45) 
	{
		createMapping(step);
		//createMapping_profile();
		//return 0;

		readSrcDonutFile();

		for(int i = 0; i < 3; i++)
		{
			createPersp();
			writePersp(step++);
			printf ("%d\n", step); 
		}
	}
	delete g_image_in;
	delete g_image_out;
	delete g_mapping;

	return 0;
}


// read out a block of data once and reuse those data as much as possible - statistics to find out how to read out block of data
